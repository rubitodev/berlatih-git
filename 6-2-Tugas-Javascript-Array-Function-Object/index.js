//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
daftarHewan.sort()
console.log(daftarHewan)

//jawaban [ '1. Tokek', '2. Komodo', '3. Cicak', '4. Ular', '5. Buaya' ]


//soal 2
function introduce(array) {
    return "Nama Saya " + array.name + ", umur saya " + array.age + " tahun, alamat saya di " + array.address + ", dan saya punya hobby yaitu " + array.hobby
}

var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan)
// Jawaban 
//"Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"



//soal 3

function hitung_huruf_vokal(str) {
    const count = str.match(/[aeiou]/gi).length;
    return count;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2) 

//jawaban
// 3 2

//soal 4
function hitung(n) {
    return 2 * n - 2
}

console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(5)) // 8

//jawaban
//-2
// 0
// 2
// 4
// 8